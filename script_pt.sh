#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=4
#SBATCH -t 04:00:00
#SBATCH --cpus-per-task=1
#SBATCH -o output-%j
#SBATCH -e error-%j
#SBATCH --exclusive

# Normal execution
  mpiexec ./PT.exe

# for DDT debugging 
# ddt --connect mpiexec ./PT.exe 